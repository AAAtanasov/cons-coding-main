# Cons Coding Main

Container for submodules

## Getting started

Postgres Docker should be started, after that running the two microservices can be started from executing "npm run start" from the **booking-system** and **user-security** folders. The front-end application can be started by running "ng serve" from the **booking-app** containing the _angular.json_. This implementation will be simplified if it is allowed and be encapsulated into docker images.

Docker was installed and can be used in the following manner:
`docker run --name postgres-nest -p 5432:5432 -e POSTRGRES_PASSWORD=postgres -d postgres`

After initial installation and the start of the project the two databases must be imported by the following script:

`-- Database: bookings

-- DROP DATABASE IF EXISTS bookings;

CREATE DATABASE bookings
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'en_US.utf8'
    LC_CTYPE = 'en_US.utf8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;
`
`-- Database: user-management

-- DROP DATABASE IF EXISTS "user-management";

CREATE DATABASE "user-management"
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'en_US.utf8'
    LC_CTYPE = 'en_US.utf8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;
`

Room information must be set up initially, after the project is started the following scritp must be executed:

`INSERT INTO room (name, "hostingCompany") VALUES  ('C01', 0), ('C02', 0), ('C03', 0), ('C04', 0), ('C05', 0), ('C06', 0), ('C07', 0), ('C08', 0), ('C09', 0), ('C010', 0), ('P01', 1), ('P02', 1), ('P03', 1), ('P04', 1), ('P05', 1), ('P06', 1), ('P07', 1), ('P08', 1), ('P09', 1), ('P010', 1)
`
***

## Description
A website based using Angular 13 for Front end, together with Angular Material and Bulma. Back end is split into 2 microservices - **booking-system** and **user-security**. User security deals with logging in and issuing of JWT Tokens, while also replicating user information into the booking-system. Booking-system deals with visualisation and management of bookings of different rooms.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Setting up a docker container for Postgres is required. Currently the users, secrets and passwords are temorarily hardcoded into the code, which must be changed, but due to other priorities this is postponed.

## Usage
For demonstration.


## Roadmap
Add tests, CI/CD, fix bugs and possibly set up a Kubernetes cluster.

## Contributing
Test project, should be a single responsibility project.


## License
MIT

## Project status
In development.

